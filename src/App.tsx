import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import WelcomePage from './components/pages/WelcomePage';

import './App.scss';

const App: React.FC = () => {
  return (
    <div className="App h-100">
        <Router>
          <Route path="/" component={WelcomePage} />
        </Router>
    </div>
  );
};

export default App;
