import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Col,
  Container,
  Row
} from 'reactstrap';
import { faCoins, faYinYang, faCogs, faChartLine } from '@fortawesome/free-solid-svg-icons';
import FeatureCard from './FeatureCard';


const WelcomePage: React.FC = () => {
  const [ isOpen, setOpen ] = useState(false);

  function toggleOpen() {
    setOpen(!isOpen);
  }

  return (
    <React.Fragment>
      <header className="App-header h-100">
        <Navbar className="welcome-navbar fixed-top" expand="md">
          <NavbarBrand href="/">StickyCut</NavbarBrand>
          <NavbarToggler onClick={toggleOpen} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/components/">Where to start</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://github.com/reactstrap/reactstrap">Try for free!</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Getting started
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Wharado!
                  </DropdownItem>
                  <DropdownItem>
                    Wharawan
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Kukumber
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
        <Container className="intro h-100 pt-5">
          <Row className="h-100 d-flex flex-column align-items-center justify-content-center text-center">
            <div>
              <h1 className="display-4 font-weight-bold">Scissors Hands</h1>
              <p>Calculate whatever you want</p>
            </div>
            <div>
              <h2>And calculate it good!</h2>
            </div>
          </Row>
        </Container>
      </header>
      <div className="why-us">
        <Container>
          <Row className="justify-content-center">
            <Col className="col-lg-8 text-center">
              <h2>Why we're good</h2>
              <p>
                StickyCut provides you a way to organize your price calulation
                You enter a set of materals you use, a number of goods you produce and any amount of parts
                Materials also can include time (which means work), so you can specify which items is more difficult
                Along with that you can insert a percentage which you want on top of each parf of SEBESTOIMOST
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <div className="features">
        <Container>
          <Row className="text-center">
            <Col>
              <h2 className="mt-0">Our main features</h2>
            </Col>
          </Row>
          <Row>
            <Col lg="3" md="6">
              <FeatureCard
                icon={faCoins}
                title="We can help you save money!"
                text="A quick example text to build on the card title and make up the bulk of the card's content"
              />
            </Col>
            <Col lg="3" md="6">
              <FeatureCard
                icon={faYinYang}
                title="Do more, work less!"
                text="Reuse your works and organize them to make adding new service as easy as clicking with your hand!"
              />
            </Col>
            <Col lg="3" md="6">
              <FeatureCard
                icon={faCogs}
                title="Easy to change!"
                text="Want to change price? Do it in one place!"
              />
            </Col>
            <Col lg="3" md="6">
              <FeatureCard
                icon={faChartLine}
                title="Analyze your success!"
                text="Watch your business using more than 10 charts!"
              />
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

export default WelcomePage;
