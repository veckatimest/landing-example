import React from "react";
import { IconDefinition } from '@fortawesome/fontawesome-common-types';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
} from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';

interface FeatureCardProps {
  icon: IconDefinition,
  title: string,
  text: string,
}

const FeatureCard: React.FC<FeatureCardProps> = ({ icon, title, text }) => {
  return (
    <Card className="feature-card text-center">
      <CardBody className="text-center">
        <span className="fa-layers fa-stack fa-5x">
          <FontAwesomeIcon icon={faCircle} color="#cc4400" />
          <FontAwesomeIcon icon={icon} inverse transform="shrink-8" />
        </span>
        <CardTitle>{title}</CardTitle>
        <CardText>{text}</CardText>
      </CardBody>
    </Card>
  );
};

export default FeatureCard;
